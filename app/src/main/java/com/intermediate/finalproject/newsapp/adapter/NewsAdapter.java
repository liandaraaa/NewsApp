package com.intermediate.finalproject.newsapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.intermediate.finalproject.newsapp.R;
import com.intermediate.finalproject.newsapp.model.News;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private ArrayList<News> news;
    private OnNewsClickCallback onNewsClickCallback;

    public OnNewsClickCallback getOnNewsClickCallback() {
        return onNewsClickCallback;
    }

    public void setOnNewsClickCallback(OnNewsClickCallback onNewsClickCallback) {
        this.onNewsClickCallback = onNewsClickCallback;
    }

    public ArrayList<News> getNews() {
        return news;
    }

    public void setNews(ArrayList<News> news) {
        this.news = news;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
       holder.bind(getNews().get(position));
    }

    @Override
    public int getItemCount() {
        return getNews().size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder{

        TextView title, author, description;
        ImageView image, favorite;
        CardView cvItem;


        public NewsViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.tv_judul_item_news);
            author = itemView.findViewById(R.id.tv_penulis_item_news);
            description = itemView.findViewById(R.id.tv_deskripsi_item_news);
            image = itemView.findViewById(R.id.iv_item_news);
            favorite = itemView.findViewById(R.id.iv_fav_item_news);
            cvItem = itemView.findViewById(R.id.cv_item_news);
        }

        public void bind(News news){
            Glide.with(itemView).load(news.getImage()).into(image);
            title.setText(news.getTitle());
            author.setText(news.getAuthor());
            description.setText(news.getDescription());
            favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    favorite.setImageResource(R.drawable.ic_fav_star);
                }
            });
            cvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getOnNewsClickCallback().onNewsClicked(getNews().get(getAdapterPosition()));
                }
            });
        }
    }

    public interface OnNewsClickCallback{
        void onNewsClicked(News news);
    }
}
