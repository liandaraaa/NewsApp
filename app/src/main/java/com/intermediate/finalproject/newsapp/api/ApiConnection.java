package com.intermediate.finalproject.newsapp.api;


import com.intermediate.finalproject.newsapp.response.NewsResponse;
import retrofit2.Call;

public class ApiConnection {

    private static final String BASE_URL =
            "https://newsapi.org/v2/";

    public static Call<NewsResponse> getNewsList(String sources, String apiKey) {
        return ApiService.createService(ApiClient.class,
                OkHttpClientFactory.create(), BASE_URL)
                .getNews(sources, apiKey);
    }


}
