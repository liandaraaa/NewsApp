package com.intermediate.finalproject.newsapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.intermediate.finalproject.newsapp.R;
import com.intermediate.finalproject.newsapp.model.News;

public class DetailActivity extends AppCompatActivity {

    public static final String NEWS = "news";
    private ImageView image;
    private Button share;
    private TextView title, author, description;
    private News news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        image = findViewById(R.id.iv_detail_news);
        share = findViewById(R.id.btn_detail_share);
        title = findViewById(R.id.tv_judul_detail_news);
        author = findViewById(R.id.tv_penulis_detail_news);
        description = findViewById(R.id.tv_deskripsi_detail_news);

        news = getIntent().getParcelableExtra(NEWS);

        Glide.with(this).load(news.getImage()).into(image);
        title.setText(news.getTitle());
        author.setText(news.getAuthor());
        description.setText(news.getDescription());
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share(news);
            }
        });


    }

    private void share(News news){
        String url = news.getImage();
        Intent share = new Intent();
        share.setAction(Intent.ACTION_SEND);
        share.putExtra(Intent.EXTRA_TITLE, news.getAuthor());
        share.putExtra(Intent.EXTRA_TEXT, url);
        share.setType("text/plain");
        startActivity(share);
    }
}
