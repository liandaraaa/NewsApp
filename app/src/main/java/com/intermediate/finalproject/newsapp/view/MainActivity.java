package com.intermediate.finalproject.newsapp.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.intermediate.finalproject.newsapp.R;
import com.intermediate.finalproject.newsapp.adapter.NewsAdapter;
import com.intermediate.finalproject.newsapp.api.ApiConnection;
import com.intermediate.finalproject.newsapp.model.News;
import com.intermediate.finalproject.newsapp.response.NewsResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NewsAdapter.OnNewsClickCallback {

    private final String KEY_RECYCLER_STATE = "recycler_state";
    private final String SOURCES = "techcrunch";
    private final String API_KEY = "5ecd2fad8e814fb2a6c1ef65acb54b37";

    private RecyclerView rvNews;
    private NewsAdapter adapterNews;
    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;

    private static Bundle bundle;
    private ProgressBar progressBar;
    private Parcelable listState = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvNews = findViewById(R.id.rv_news);
        progressBar = findViewById(R.id.pb_news);

        linearLayoutManager = new LinearLayoutManager(this);
        gridLayoutManager = new GridLayoutManager(this, 2);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            rvNews.setLayoutManager(linearLayoutManager);
        }
        else {
            rvNews.setLayoutManager(gridLayoutManager);
        }

        rvNews.setHasFixedSize(true);

        adapterNews = new NewsAdapter();
        adapterNews.setNews(new ArrayList<News>());
        rvNews.setAdapter(adapterNews);
        adapterNews.setOnNewsClickCallback(this);

        progressBar.setVisibility(View.VISIBLE);

        ApiConnection.getNewsList(SOURCES, API_KEY)
                .enqueue(new Callback<NewsResponse>() {
                    @Override
                    public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                        progressBar.setVisibility(View.GONE);
                        adapterNews.setNews(response.body().getNews());
                        adapterNews.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<NewsResponse> call, Throwable t) {

                        progressBar.setVisibility(View.GONE);
                        toast(getString(R.string.label_warning));
                    }
                });

    }

    private void toast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (bundle != null){
            listState = bundle.getParcelable(KEY_RECYCLER_STATE);
            rvNews.getLayoutManager().onRestoreInstanceState(listState);
        }

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            rvNews.setLayoutManager(gridLayoutManager);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            rvNews.setLayoutManager(linearLayoutManager);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        bundle = new Bundle();
        listState = rvNews.getLayoutManager().onSaveInstanceState();
        bundle.putParcelable(KEY_RECYCLER_STATE, listState);
    }

    @Override
    public void onNewsClicked(News news) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(DetailActivity.NEWS,news);
        startActivity(intent);

    }
}
