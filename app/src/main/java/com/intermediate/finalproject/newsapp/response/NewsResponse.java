package com.intermediate.finalproject.newsapp.response;

import com.google.gson.annotations.SerializedName;
import com.intermediate.finalproject.newsapp.model.News;

import java.util.ArrayList;

public class NewsResponse {

    @SerializedName("articles")
    private ArrayList<News> news = new ArrayList<>();

    public ArrayList<News> getNews() {
        return news;
    }

    public void setNews(ArrayList<News> news){
        this.news = news ;
    }
}
