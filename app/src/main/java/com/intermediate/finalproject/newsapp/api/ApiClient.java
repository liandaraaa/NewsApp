package com.intermediate.finalproject.newsapp.api;

import com.intermediate.finalproject.newsapp.response.NewsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiClient {

    @GET("top-headlines")
    Call<NewsResponse> getNews(@Query("sources") String sources, @Query("apiKey") String apiKey );
}
